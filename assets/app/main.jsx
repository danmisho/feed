var React = require("react");
var ReactDOM = require("react-dom");
var io = require('socket.io-client')

// var ReactMotion = require('react-motion');
// var Motion = ReactMotion.Motion;

var Homepage = require("./containers/Homepage.jsx");

ReactDOM.render(<Homepage />,  document.getElementById('root'));
