var React = require("react");

var Header = require('../components/Header.jsx')
var Feed = require('../components/Feed.jsx')

var Homepage = React.createClass({
  render: function() {
      return (
        <div className="homepage">
          <Header />
          <Feed />
        </div>
      )
    }
});

module.exports = Homepage
