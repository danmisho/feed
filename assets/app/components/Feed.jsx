var React = require("react");
var io = require('socket.io-client')

var Posts = require('./Posts.jsx');

var Feed = React.createClass({
  getInitialState: function() {
    return {
      posts: []
    };
  },
  componentWillMount: function(){
    this.socket = io('http://localhost:3000');
    this.socket.on('connect', this.connect)

    this.socket.on('post', (data) => {
      this.addPost(data)
    });
  },
  connect: function() {
    console.log('Connected to server, fetching data...');
  },
  addPost: function(e) {
    var newArray = this.state.posts;
    newArray.unshift(
      {
        name: e.name,
        age: e.age,
        avatar: e.avatar,
        key: Date.now()
      }
    );
    this.setState({posts:newArray})
  },
  render: function() {
      var self = this;
      return (
        <div className="post-list container">
          <Posts entries={this.state.posts}/>
        </div>
      );
    }
});

module.exports = Feed
