var React = require("react");
var io = require('socket.io-client')

var FlipMove = require('react-flip-move')

var Posts = React.createClass({
  render: function() {
    var postEntries = this.props.entries;
    function pushPosts(post) {
      return (
        <div key={post.key} className="col-md-6">
          <div className="post">
            <div className="post-header">
              <div className="post-name"><span>{post.name}</span></div>
              <div className="post-age"><span>{post.age}</span></div>
            </div>
            <div className="post-avatar">
              <img src={post.avatar} />
            </div>
          </div>
        </div>
      )
    }
    var listPosts = postEntries.map(pushPosts);
    return (
      <div className="theList">
	  <FlipMove easing="ease-in">
              {listPosts} 
          </FlipMove>
      </div>
    );
  }
});

module.exports = Posts
