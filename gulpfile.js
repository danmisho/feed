var gulp = require('gulp');
var sass = require('gulp-sass');
var image = require('gulp-image');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var source = require("vinyl-source-stream");
var spawn = require('child_process').spawn;
var runSequence = require('run-sequence');
var babelify = require('babelify');
var browserify = require('browserify');

gulp.task('build', function () {
  return browserify({
    entries: './assets/app/main.jsx',
    extensions: ['.jsx'],
    debug: true
  })
  .transform(babelify.configure({presets: ["es2015", "react"]}))
  .bundle()
  .pipe(source('main.js'))
  .pipe(gulp.dest('dist'));
});

gulp.task('compress', function() {
  return gulp.src('./dist/main.js')
    .pipe(uglify())
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest('dist'));
});

gulp.task('sass', function () {
  return gulp.src('./assets/styles/main.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./dist/css'));
});

gulp.task('sass:watch', function () {
  gulp.watch('./assets/styles/main.scss', ['sass']);
});

gulp.task('image', function () {
  gulp.src('./assets/img/*')
    .pipe(image())
    .pipe(gulp.dest('./dist/img'));
});

gulp.task('server', function() {
  spawn('node', ['index.js'], { stdio: 'inherit' });
});

gulp.task('default', function(cb) {
  runSequence('build', 'server', 'sass', 'image', cb);
});
